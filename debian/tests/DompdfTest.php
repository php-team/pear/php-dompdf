<?php

declare(strict_types = 1);

use \PHPUnit\Framework\TestCase;
use \Dompdf\Dompdf;

class DompdfTest extends TestCase
{

    public function testConstruct(): void
    {
        $dompdf = new Dompdf();

        // Load HTML content
        $dompdf->load_html('<!DOCTYPE html>
        <html lang="fr">
        <head>
        <title>Page de Test HTML – dompdf, un outil puissant pour convertir de l’HTML vers PDF en PHP</title>
        </head>

        <body>
        <p>
        Cette page <em>HTML</em> va être convertie à l’aide de <em>dompdf</em> en <em>PDF</em>
        </p>
        </body>
        </html>');

        // Render the HTML as PDF
        $dompdf->render();
        $output = $dompdf->output();

        $this->assertStringContainsString('%PDF-1.7', $output);
        $this->assertStringContainsString('/Producer', $output);
        $this->assertStringContainsString('/Title', $output);
    }
}
