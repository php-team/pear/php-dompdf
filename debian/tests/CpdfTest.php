<?php

declare(strict_types = 1);

use \PHPUnit\Framework\TestCase;
use \Dompdf\Cpdf;

class CpdfTest extends TestCase
{

    public function testConstruct(): void
    {
        $cpdf = new Cpdf();
        $this->assertSame(7, $cpdf->getFirstPageId());
        $this->assertSame('1.7', Cpdf::PDF_VERSION);
    }
}
