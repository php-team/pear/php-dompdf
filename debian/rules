#!/usr/bin/make -f

%:
	dh $@

DEB_DIR=$(CURDIR)/debian/php-dompdf

execute_before_dh_auto_build:
	# Link all fonts that would be linked at install time
	debian/bin/install-local-fonts.sh

	# Make the missing ufm files
	debian/bin/ttf2ufm lib/fonts/DejaVuSans-Bold.ttf lib/fonts/DejaVuSans-Bold.ufm
	debian/bin/ttf2ufm lib/fonts/DejaVuSans-BoldOblique.ttf lib/fonts/DejaVuSans-BoldOblique.ufm
	debian/bin/ttf2ufm lib/fonts/DejaVuSans-Oblique.ttf lib/fonts/DejaVuSans-Oblique.ufm
	debian/bin/ttf2ufm lib/fonts/DejaVuSans.ttf lib/fonts/DejaVuSans.ufm
	debian/bin/ttf2ufm lib/fonts/DejaVuSansMono-Bold.ttf lib/fonts/DejaVuSansMono-Bold.ufm
	debian/bin/ttf2ufm lib/fonts/DejaVuSansMono-BoldOblique.ttf lib/fonts/DejaVuSansMono-BoldOblique.ufm
	debian/bin/ttf2ufm lib/fonts/DejaVuSansMono-Oblique.ttf lib/fonts/DejaVuSansMono-Oblique.ufm
	debian/bin/ttf2ufm lib/fonts/DejaVuSansMono.ttf lib/fonts/DejaVuSansMono.ufm
	debian/bin/ttf2ufm lib/fonts/DejaVuSerif-Bold.ttf lib/fonts/DejaVuSerif-Bold.ufm
	debian/bin/ttf2ufm lib/fonts/DejaVuSerif-BoldItalic.ttf lib/fonts/DejaVuSerif-BoldItalic.ufm
	debian/bin/ttf2ufm lib/fonts/DejaVuSerif-Italic.ttf lib/fonts/DejaVuSerif-Italic.ufm
	debian/bin/ttf2ufm lib/fonts/DejaVuSerif.ttf lib/fonts/DejaVuSerif.ufm

	# The file lib/fonts/installed-fonts.dist.json was dfsg removed, add it back
	# Last checked: 07/2024 for 3.0.0, in sync with upstream
	debian/bin/create-fonts-list.sh

override_dh_auto_build:
	phpab --output src/autoload.php \
		--template debian/autoload.package.php.tpl \
		src/

override_dh_auto_test:
	phpabtpl --require-file ../lib/Cpdf.php composer.json > debian/autoload.php.tpl
	mkdir -p vendor
	phpab --output vendor/autoload.php \
		--template debian/autoload.php.tpl \
		src/
	phpabtpl \
		--require mockery/mockery --require symfony/process \
		--require-file ../vendor/autoload.php > debian/autoload.tests.php.tpl
	phpab --output tests/autoload.php \
		--template debian/autoload.tests.php.tpl \
		tests/

	phpunit --no-coverage  --do-not-cache-result --bootstrap tests/autoload.php
	# Dompdf test requires installed-fonts.dist.json in the installed paths
	phpunit --no-coverage  --do-not-cache-result --bootstrap tests/autoload.php debian/tests/CpdfTest.php

override_dh_installchangelogs:
	dh_installchangelogs debian/upstream/changelog

override_dh_fixperms:
	dh_fixperms
	#chown www-data:www-data	$(DEB_DIR)/var/cache/php-dompdf/fonts
	chmod 2775		$(DEB_DIR)/var/cache/php-dompdf/fonts
	chmod 2775		$(DEB_DIR)/var/cache/php-dompdf/tmp
