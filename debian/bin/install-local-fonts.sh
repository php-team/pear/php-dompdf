#!/bin/sh

set -eu

sed  's,usr/share/php/dompdf/,,' debian/links | while read line
do
  echo "Linking: /$line"
  ln -s /$line
done
